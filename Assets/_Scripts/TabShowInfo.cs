﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabShowInfo : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.Tab))
        {
            this.gameObject.GetComponent<Text>().enabled = true;
            
        }
        else
        {
            this.gameObject.GetComponent<Text>().enabled = false;

        }
    }
}
