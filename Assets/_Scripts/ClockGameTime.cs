using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClockGameTime : MonoBehaviour
{

    public GameObject minuteHand;
    public GameObject hourHand;

    private float minuteDeg;
    private float hourDeg;

    private int minute;
    private int hour;
    private string AMPM;
    private string minuteString;
    private string hourString;


    // Start is called before the first frame update
    void Start()
    {
        AMPM = "AM";
        minuteDeg = 0;
        hourDeg = 0;

        print("calling invokerepeating!");
        InvokeRepeating("time", 0, 1);
    }

    // Update the time
    void time()
    {
        minuteDeg -= 12;
        hourDeg -= 1;
        minuteHand.transform.eulerAngles = new Vector3(0, 0, minuteDeg);
        hourHand.transform.eulerAngles = new Vector3(0, 0, hourDeg);

        minute = (int)(-(minuteDeg %360)/ 6);
        hour = (int)(-(hourDeg %360) / 30);
        if(minute == 59 && hour == 11)
        {
            if(AMPM == "AM")
            {
                AMPM = "PM";
            } else
            {
                AMPM = "AM";
            }
        }

        minuteString = minute.ToString();
        hourString = hour.ToString();

        //print("hour is: " + hour);

        if (minute < 10)
        {
            minuteString = "0" + minuteString;
        }
        if (hour < 10)
        {
            hourString = "0" + hourString;
        }


        print("Time is: " + hourString + " : " + minuteString + " " + AMPM);
    }
}
