﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabCalendar : MonoBehaviour
{
	public GameObject calendar;

    void start()
	{
		calendar = GameObject.Find("calendar");
	}

	//Update is called once per frame
	void Update()
	{

		if (Input.GetKey(KeyCode.Tab))
		{
			calendar.SetActive(true);

		}
		else
		{
			calendar.SetActive(false);

		}
	}
}
