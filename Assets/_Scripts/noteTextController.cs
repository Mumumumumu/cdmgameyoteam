﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class noteTextController : MonoBehaviour
{

    public GameObject FPSController;
    public GameObject noteCube;
    public GameObject noteImage;

    // Start is called before the first frame update
    void Start()
    {
        FPSController = GameObject.Find("FPSController");
        noteCube = GameObject.Find("note");
        noteImage = GameObject.Find("noteImage");
        noteImage.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(FPSController.transform.position, noteCube.transform.position) <= 1.5f){
            this.gameObject.GetComponent<Text>().enabled = true;

            //click to display the note
            if (Input.GetKey(KeyCode.Mouse0))
            {
                noteImage.SetActive(true);
            } else
            {
                noteImage.SetActive(false);
            }

        } else
        {
            this.gameObject.GetComponent<Text>().enabled = false;
            noteImage.SetActive(false);
        }
    }
}
