//Make an empty GameObject and call it "Door"

//Drag and drop your Door model into Scene and rename it to "Body"
//Make sure that the "Door" Object is at the side of the "Body" object (The place where a Door Hinge should be)

//Move the "Body" Object inside "Door"

//Add a Collider (preferably SphereCollider) to "Door" object and make it much bigger then the "Body" model, mark it as Trigger


//Assign this script to a "Door" Object (the one with a Trigger Collider)
//Make sure the main Character is tagged "Player"
//Upon walking into trigger area press "F" to open / close the door

using UnityEngine;

public class OpenableDoor : MonoBehaviour
{

    // Smoothly open a door
    public float doorOpenAngle = 90.0f; //Set either positive or negative number to open the door inwards or outwards
    public float openSpeed = 2.0f; //Increasing this value will make the door open faster

    bool open = false;
    bool enter = false;

    float defaultRotationAngle;
    float currentRotationAngle;
    float openTime = 0;

    bool isKeyRetrieved = false;


    public GameObject LockedDoorIndicator;
    public GameObject EnabledDoorIndicator;


    AudioSource DoorAudioSource;
    public AudioClip AtemptedClip;
    public AudioClip OpenedClip;





    void Start()
    {
        defaultRotationAngle = transform.localEulerAngles.y;
        currentRotationAngle = transform.localEulerAngles.y;


        DoorAudioSource = GetComponent<AudioSource>();
    }

    // Main function
    void Update()
    {
        if (isKeyRetrieved == true)
        {
            DoorOpen();
        }

    }



    public void EnableDoor()
    {
        isKeyRetrieved = true;
        Debug.Log("Door is enabled");
    }





    private void DoorOpen()
    {
        if (openTime < 1)
        {
            openTime += Time.deltaTime * openSpeed;
        }
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, Mathf.LerpAngle(currentRotationAngle, defaultRotationAngle + (open ? doorOpenAngle : 0), openTime), transform.localEulerAngles.z);

        if (Input.GetKeyDown(KeyCode.E) && enter)
        {
            open = !open;
            currentRotationAngle = transform.localEulerAngles.y;
            openTime = 0;

            DoorAudioSource.PlayOneShot(OpenedClip, 1);

        }
    }

    // Display a simple info message when player is inside the trigger area
    void OnGUI()
    {
        if (enter)
        {
            if (isKeyRetrieved == false)
            {
                //GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height - 100, 150, 30), "This door is locked. You need a key");
                LockedDoorIndicator.SetActive(true);

            }

            else
            {
                //GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height - 100, 150, 30), "Press 'E' to open the door");
                EnabledDoorIndicator.SetActive(true);
            }
        }


        if (enter == false)
        {
            LockedDoorIndicator.SetActive(false);
            EnabledDoorIndicator.SetActive(false);
        }
    }

    // Activate the Main function when Player enter the trigger area
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enter = true;


            if(isKeyRetrieved == false)
            {
                DoorAudioSource.PlayOneShot(AtemptedClip, 1);

            }
        }
    }

    // Deactivate the Main function when Player exit the trigger area
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enter = false;
        }
    }
}
