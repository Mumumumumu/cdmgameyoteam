﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedeemChest : MonoBehaviour
{

    public GameObject IsKeyretrieved;
    public GameObject SecondTicker;

    AudioSource WinningAudioSource;
    public AudioClip WinningClip;



    // Start is called before the first frame update
    void Start()
    {
        WinningAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame


    void Update()
    {


    }




    private void OnTriggerEnter(Collider other)
    {

        if (IsKeyretrieved.activeInHierarchy)
        {
            SecondTicker.SetActive(true);
            WinningAudioSource.PlayOneShot(WinningClip, 1);

        }

    }
}
