﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1.0f;
    public float displayImageDuration = 1.0f;
    public GameObject player;


    //public CanvasGroup BeCaughtCanvasImageCanvasGroup;

    //Player get caught
    public CanvasGroup caughtBackgroundImageCanvasGroup;

    //Played starved to death
    public CanvasGroup starvedBackgroundImageCanvasGroup;

    //game winning
    public CanvasGroup winningCanvasGroup;



    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    bool m_IsPlayerStarved = false;

    float m_Timer;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }

    /*
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }
    */

    public void StarvedPlayer()
    {
        m_IsPlayerStarved = true;
    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }




    void Update()
    {


        //Caught Situation
        if (m_IsPlayerAtExit)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, false);
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true);
            Debug.Log("Displayed caught image");
        }


        //Starve Situation
        if (m_IsPlayerStarved == true)
        {
            EndLevel(starvedBackgroundImageCanvasGroup, true);
            Debug.Log("Player staved");

        }




        ReloadSceneTrigger();
    }

    private static void ReloadSceneTrigger()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            print("Return key was pressed");
            SceneManager.LoadScene(0);
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart)
    {
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;



        /*
         
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }

        */
    }
}
